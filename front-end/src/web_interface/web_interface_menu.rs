// use std::str::FromStr;

use askama::Template;
use axum::{
    response::{Html, IntoResponse}
};

// Menu page
#[derive(Template)]
#[template(path = "menu.html")]
struct MenuTemplate {
}

pub async fn menu() -> impl IntoResponse {

    let template = MenuTemplate {
    };

    let html = template.render().unwrap();

    Html(html)
}
