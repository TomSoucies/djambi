
use axum::{
  routing::get,
  Router
};
// use axum_extra::routing::SpaRouter;

use std::{net::SocketAddr};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod web_interface;
// use web_interface::web_interface_menu;

#[tokio::main]
async fn main() {
  tracing_subscriber::registry()
      .with(
          tracing_subscriber::EnvFilter::try_from_default_env()
              .unwrap_or_else(|_| "example_templates=debug".into()),
      )
      .with(tracing_subscriber::fmt::layer())
      .init();

  // build our application with a route
  let app = Router::new()
      .route("/", get(web_interface::web_interface_menu::menu));

  // run our app with hyper
  // `axum::Server` is a re-export of `hyper::Server`
  let addr = SocketAddr::from(([127, 0, 0, 1], 8000));
  tracing::debug!("listening on {}", addr);
  match open::that("http://localhost:8000/"){
      Ok(_) => {},
      Err(err) => tracing::error!("Failed to open browser. Error: {}", err),
  }

  axum::Server::bind(&addr)
      .serve(app.into_make_service_with_connect_info::<SocketAddr>())
      .await
      .unwrap();

}
