// Editor buttons
const run_btn = document.querySelector("#run-btn");
const stop_btn = document.querySelector("#stop-btn");
// Hide stop button
stop_btn.style.display = "none";

// Log textarea
const textarea = document.querySelector("#log-textarea");

// Output svg
const output = document.getElementById('microworld-output');
const control_play_pause = document.getElementById('control-play-pause');
const control_slider = document.getElementById('control-slider');
const control_track = document.getElementsByClassName('control-slider-range')[0];
const control_speed = document.getElementById('control-speed');

var svg_cache = [""];
var svg_index = 0;
var is_paused = false;
var speed = 2;
var last_time = 0;

function switch_pause(force_pause = false) {
    let i = control_play_pause.getElementsByTagName("i")[0];
    if (is_paused && !force_pause) {
        is_paused = false;
        i.innerHTML = "pause";
    } else {
        is_paused = true;
        i.innerHTML = "play_arrow";
    }
}

control_play_pause.addEventListener("click", function (e) {
    // Get i inside control_play_pause
    switch_pause();
});

control_slider.addEventListener("change", function (e) {
    switch_pause(true);
    svg_index = parseInt(this.value);
    draw_svg(skip_time = true, step=false);
});

control_speed.addEventListener("change", function (e) {
    speed = this.value;
});

function add_svg_to_cache(svg) {
    // Add svg to cache
    svg_cache.push(svg);
    control_slider.max = svg_cache.length - 1;
    control_track.style.setProperty("--max", svg_cache.length - 1);
    if (svg_cache.length > 30) {
        control_track.style.setProperty("--step", 5);
    }
}

function draw_svg(skip_time = false, step=true) {
    // Draw svg and take time
    var time = performance.now();
    // If time is greater than 1000 / speed
    if (svg_index+1<svg_cache.length && (skip_time || (time - last_time > 1000 / speed))) {
        // Update last time
        last_time = time;
        // If not paused
        output.innerHTML = svg_cache[svg_index];
        control_slider.value = svg_index;
        if (step)
            svg_index += 1;
    }
}

// Event every 30ms
setInterval(function () {
    // If not paused
    if (!is_paused) {
        // Draw svg
        draw_svg();
    }
}, 30);

websocket.onopen = function () {
    console.log("connection opened");
}

websocket.onclose = function () {
    console.log("connection closed");
}

websocket.onmessage = function (e) {
    parse_json(e.data);
}

websocket.onerror = function (e) {
    console.log("error: " + e.data);
}

run_btn.addEventListener("click", function (e) {
    this.style.display = "none";
    stop_btn.style.display = "block";
    // Send json to server
    svg_cache = [""];
    svg_index = 0;
    websocket.send(JSON.stringify({
        "type": "start",
        "code": editor.getValue()
    }));
});

stop_btn.addEventListener("click", function (e) {
    this.style.display = "none";
    run_btn.style.display = "block";
    // Send json to server
    websocket.send(JSON.stringify({
        "type": "stop"
    }));
});


let parse_json = function (json) {
    // parse json string
    json = JSON.parse(json);
    // match type of json
    switch (json["type"]) {
        case "svg":
            add_svg_to_cache(json["content"]);
            break;
        case "log":
            textarea.innerHTML += "[LOG] " + json["content"];
            // add new line
            textarea.innerHTML += "<br>";
            break;
        case "error":
            textarea.innerHTML += "<span class=\"error\"> [ERROR] " + json["content"];
            // add new line
            textarea.innerHTML += "</span><br>";
            break;
        case "control":
            handle_control(json["content"]);
            break;
    }
}

function handle_control(content) {
    switch (content) {
        case "end":
            run_btn.style.display = "block";
            stop_btn.style.display = "none";
            break;
    }
}